# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
# Copyright (c) 2015 M. Dietrich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the 'Software'), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# TODO add http://paste.ubuntu.com/12524217/
# consider https://github.com/hallyn/qtile-config/blob/master/config.py

from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, hook, bar, widget
import os

mod = 'mod4'
color_alert = '#36af90'
color_frame = '#7f9181'
qtile_dizini = os.path.expanduser("~/.config/qtile/")
komut_menu = "python3 "+qtile_dizini + 'mmenu'
parlaklik = 1.0
ses = "mute"

# kick a window to another screen (handy during presentations)
def kick_to_next_screen(qtile, direction=1):
	other_scr_index = (qtile.screens.index(qtile.currentScreen) + direction) % len(qtile.screens)
	othergroup = None
	for group in qtile.cmd_groups().values():
		if group['screen'] == other_scr_index:
			othergroup = group['name']
			break
	if othergroup:
		qtile.moveToGroup(othergroup)

def parlaklik_azalt(qtile,direction=1):
	global parlaklik
	parlaklik -= 0.1
	if parlaklik >= 0.0:
		os.system("xrandr --output eDP1 --brightness {}".format(parlaklik))
	else:
		parlaklik = 0.0

def parlaklik_artir(qtile,direction=1):
	global parlaklik
	parlaklik += 0.1
	if parlaklik <= 1.1:
		os.system("xrandr --output eDP1 --brightness {}".format(parlaklik))
	else:
		parlaklik = 1.0

def ses_mute_unmute(qtile,direction=1):
	global ses
	if ses == "mute":
		ses = "unmute"
		os.system("amixer set Master unmute")
	elif ses == "unmute":
		ses = "mute"
		os.system("amixer set Master mute")

# future use: udev code
def __x():
	import pyudev
	context = pyudev.Context()
	monitor = pyudev.Monitor.from_netlink(context)
	monitor.filter_by('drm')
	monitor.enable_receiving()
	observer = pyudev.MonitorObserver(monitor, setup_monitors)
	observer.start()


# see http://docs.qtile.org/en/latest/manual/config/keys.html
keys = [
	Key([mod], 'z', lazy.spawn(komut_menu)),
	# Switch between windows in current stack pane
	Key([mod], 'Tab', lazy.layout.down()),
	Key([mod, 'shift'], 'Tab', lazy.layout.up()),
	# Move windows up or down in current stack
	Key([mod, 'mod1'], 'Tab', lazy.layout.shuffle_down()),
	Key([mod, 'mod1', 'shift'], 'Tab', lazy.layout.shuffle_up()),
	# Switch window focus to other pane(s) of stack
	Key([mod, 'control'], 'Tab', lazy.layout.next()),
	Key([mod, 'control', 'shift'], 'Tab', lazy.layout.prev()),
	Key([], 'Print', lazy.spawn("scrot '/tmp/%Y-%m-%d_$wx$h_scrot.png' -e 'xdg-open $f'")),

	Key([mod], "o", lazy.function(kick_to_next_screen)),
	Key([mod, "shift"], "o", lazy.function(kick_to_next_screen, -1)),
	Key([mod], 'Return', lazy.spawn('xfce4-terminal')),
	Key([mod], 'l', lazy.spawn('xlock')),
	Key([], 'XF86Launch1', lazy.spawn('xlock')),

	Key([], 'XF86MonBrightnessUp', lazy.function(parlaklik_artir)),
	Key([], 'XF86MonBrightnessDown', lazy.function(parlaklik_azalt)),

	Key([], 'XF86AudioMute', lazy.function(ses_mute_unmute)),
	Key([], 'XF86AudioMicMute', lazy.spawn('amixer -D pulse set Master toggle')),
	Key([], 'XF86AudioRaiseVolume', lazy.spawn('amixer -c 0 -q set Master 2dB+')),
	Key([], 'XF86AudioLowerVolume', lazy.spawn('amixer -c 0 -q set Master 2dB-')),

	#Uygulama kısayolları
	Key([mod], 't', lazy.spawn('thunar')),
	Key([mod], 'f', lazy.spawn('falkon')),
	Key([mod], 'x', lazy.spawn('xfce4-terminal')),
	Key([mod], 'k', lazy.spawn('karpuz.py')),
	Key([mod], 's', lazy.spawn('sylpheed')),
	Key([mod], 'g', lazy.spawn('gimp')),
	Key([mod], 'i', lazy.spawn('inkscape')),
	Key([mod], 'b', lazy.spawn('blender')),
	Key([mod], 'a', lazy.spawn('audacity')),
	Key([mod], 'v', lazy.spawn('vlc')),
	Key([mod], 'p', lazy.spawn('pitivi')),


	# Switch groups
	Key([], 'XF86Back', lazy.screen.prev_group(skip_managed=True, )),
	Key([], 'XF86Forward', lazy.screen.next_group(skip_managed=True, )),
	Key([mod], 'XF86Back', lazy.screen.prev_group(skip_managed=True, )),
	Key([mod], 'XF86Forward', lazy.screen.next_group(skip_managed=True, )),
	Key([mod], 'Left', lazy.screen.prev_group(skip_managed=True, )),
	Key([mod], 'Right', lazy.screen.next_group(skip_managed=True, )),
	Key([mod], 'Escape', lazy.screen.togglegroup()),
	# Toggle between different layouts as defined below
	Key([mod], 'space', lazy.next_layout()),
	Key([mod, 'shift'], 'space', lazy.prev_layout()),
	Key([mod, 'shift'], 'q', lazy.window.kill()),
	# qtile maintenence
	Key([mod, 'shift'], 'r', lazy.restart()), # default is control! ;)
	Key([mod, 'shift'], 's', lazy.shutdown()),
	Key([mod], 'r', lazy.spawncmd()),
	Key([mod], 'n', lazy.window.toggle_minimize()),
	]

# create groups
groups = [Group("1 ", matches=[Match(wm_class=["xterm","Xfce4-terminal"])],layout="floating"),
		Group("2 ", matches=[Match(wm_class=["Thunar"])],layout="max"),
		Group("3 ", matches=[Match(wm_class=["firefox","Falkon"])],layout="max"),
		Group("4 ", matches=[Match(wm_class=["Karpuz.py","Geany"])],layout="max"),
		Group("5 ", matches=[Match(wm_class=["XpdfReader","libreoffice-startcenter","libreoffice-writer","libreoffice-calc","libreoffice"
											"libreoffice-base","libreoffice-draw","libreoffice-impress","libreoffice-math"])],layout="monadtall"),
		Group("6 ", matches=[Match(wm_class=["Gimp","Inkscape","Blender"])],layout="monadtall"),
		Group("7 ", matches=[Match(wm_class=["Audacity"])],layout="max"),
		Group("8 ", matches=[Match(wm_class=["vlc","Pitivi"])],layout="monadtall"),
		Group("9 ", matches=[Match(wm_class=["Sylpheed"])],layout="max"),]
for i in groups:
	# mod1 + letter of group = switch to group
	g_name = i.name
	g_name = g_name.split()[0]
	keys.append(
		Key([mod], g_name, lazy.group[i.name].toscreen())
	)

	# mod1 + shift + letter of group = switch to & move focused window to group
	keys.append(
		Key([mod, 'shift'], g_name, lazy.window.togroup(i.name))
	)

# see http://docs.qtile.org/en/latest/manual/ref/layouts.html
layouts = [
	layout.MonadTall(margin=4, border_width=2, border_focus=color_alert, border_normal=color_frame),
	layout.Max(),
	layout.Floating(border_focus=color_alert, border_normal=color_frame, ),
	#layout.Matrix(),
	#layout.RatioTile(),
	#layout.Slice(),
	#layout.Stack(num_stacks=2),
	layout.Tile(border_focus=color_alert, border_normal=color_frame, ),
	#layout.TreeTab(),
	#layout.VerticalTile(),
	#layout.Zoomy(),
	#layout.Bsp(),
	]

widget_defaults = dict(
	font='Sans',
	fontsize=14,
	background="#272822",
	foreground="#cfd0c2",
	)

# see http://docs.qtile.org/en/latest/manual/ref/widgets.html
screens = [Screen(top=bar.Bar([
	widget.LaunchBar(
		progs = [("Komut","qshell:self.qtile.cmd_spawncmd()","Komut")],
		default_icon=qtile_dizini+"/icons/folder-script.png",
		padding = 5,
		),
	widget.Prompt(prompt="komut: "),
	widget.GroupBox(
		active="#cfd0c2",
		inactive="#76715e",
		highlight_color=[color_alert,"#76715e"],
		disable_drag=True,
		this_current_screen_border=color_frame,
		this_screen_border=color_frame,
		urgent_text=color_alert,
		),
	widget.CurrentLayout(),
	widget.WindowName(),
	widget.Notify(),
	widget.Systray(),
	widget.Image(filename=qtile_dizini+"/icons/wlan.png"),
	widget.Wlan(interface="wlp2s0",
		format="{quality}/70",
		padding=0,),
	widget.Volume(theme_path="/usr/share/icons/milis-icon-theme/24x24/panel",
		update_interval=0.5,
		volume_app="pulseaudio",
		padding=0),
	widget.BatteryIcon(theme_path="/usr/share/icons/milis-icon-theme/24x24/panel",
		padding=0),
	widget.Battery(
		format="{percent:2.0%}",
		padding=0,
		),
	widget.CPUGraph(
		graph_color=color_alert,
		fill_color='{}.5'.format(color_alert),
		border_color=color_frame,
		line_width=2,
		border_width=1,
		samples=60,
		width=45,
		),
	widget.MemoryGraph(
		graph_color=color_alert,
		fill_color='{}.5'.format(color_alert),
		border_color=color_frame,
		line_width=2,
		border_width=1,
		samples=60,
		width=45,
		),
	widget.NetGraph(
		graph_color=color_alert,
		fill_color='{}.5'.format(color_alert),
		border_color=color_frame,
		line_width=2,
		border_width=1,
		samples=60,
		width=45,
		),
	widget.Image(filename=qtile_dizini+"/icons/zaman.png"),
	widget.Clock(format='%Y-%m-%d %H:%M'),
	widget.Image(filename=qtile_dizini+"/icons/wp.png"),
	widget.Wallpaper(directory=qtile_dizini+"/arkaplanlar/"),
	widget.LaunchBar(
		progs = [("Kapat","qshell:self.qtile.cmd_shutdown()","Kapat")],
		default_icon=qtile_dizini+"/icons/kapat.png",
		padding = 5,
		),
	], size=24,background="#272822", ), ), ]

def detect_screens(qtile):
	while len(screens) < len(qtile.conn.pseudoscreens):
		screens.append(Screen(
		top=bar.Bar([
			widget.GroupBox(
				disable_drag=True,
				this_current_screen_border=color_frame,
				this_screen_border=color_frame,
				),
			widget.CurrentLayout(),
			widget.TaskList(
				font='Nimbus Sans L',
				border=color_frame,
				highlight_method='block',
				max_title_width=800,
				urgent_border=color_alert,
				),
			], 32, ), ))

# Drag floating layouts.
mouse = [
	Drag([mod], 'Button1', lazy.window.set_position_floating(), start=lazy.window.get_position()),
	Drag([mod], 'Button3', lazy.window.set_size_floating(), start=lazy.window.get_size()),
	Click([mod], 'Button2', lazy.window.bring_to_front())
	]

# subscribe for change of screen setup, just restart if called
@hook.subscribe.screen_change
def restart_on_randr(qtile, ev):
	# TODO only if numbers of screens changed
	qtile.cmd_restart()

def start_commands():
	os.system("setxkbmap tr &")
	os.system("pulseaudio -D &")
	#os.system("/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &")
	os.system("sylpheed &")


dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
	border_focus=color_alert,
	border_normal=color_frame,
	float_rules=[dict(role='buddy_list', ), ],
	)
auto_fullscreen = True
# java app don't work correctly if the wmname isn't set to a name that happens to
# be on java's whitelist (LG3D is a 3D non-reparenting WM written in java).
wmname = 'LG3D'
#Başlangıç komutları
start_commands()

def main(qtile):
	detect_screens(qtile)
